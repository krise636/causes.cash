## Changelog

## v20.8.4

CID: __Qm__  
Pubic Gateways: __[ipfs.io](https://ipfs.io/ipfs/QmQDfWErsgVkaK9osjvVnDnnLyFN6tctxvbFZLiiUWE5VN/)__ | __[cloudflare-ipfs.com](https://cloudflare-ipfs.com/ipfs/QmQDfWErsgVkaK9osjvVnDnnLyFN6tctxvbFZLiiUWE5VN/)__ | __[infura.io](https://ipfs.infura.io/ipfs/QmQDfWErsgVkaK9osjvVnDnnLyFN6tctxvbFZLiiUWE5VN/)__

- Flipstarter campaign support
- Mecenas recurring payment support

## v20.4.27

CID: __QmQDfWErsgVkaK9osjvVnDnnLyFN6tctxvbFZLiiUWE5VN__  
Pubic Gateways: __[ipfs.io](https://ipfs.io/ipfs/QmQDfWErsgVkaK9osjvVnDnnLyFN6tctxvbFZLiiUWE5VN/)__ | __[cloudflare-ipfs.com](https://cloudflare-ipfs.com/ipfs/QmQDfWErsgVkaK9osjvVnDnnLyFN6tctxvbFZLiiUWE5VN/)__ | __[infura.io](https://ipfs.infura.io/ipfs/QmQDfWErsgVkaK9osjvVnDnnLyFN6tctxvbFZLiiUWE5VN/)__

- First preview
